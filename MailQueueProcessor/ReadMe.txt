﻿------------------------------------------------------------------------------------------------------------------------------------------
BATCH INSTALL
------------------------------------------------------------------------------------------------------------------------------------------

To simplify installation, you can run the included batch file, InstallNlpMailQueueService.bat. This will be copied to the output directory with the exe. 
At the top of the batch file there are the following variable to check and update before running. 

DOTNETFX2
This is the path to the InstallUtil.exe on the machine we are installing the service on. 

EXEPATH
This is the path to the location of the batch file and the service install files, exe's, and config's.

SERVICENAME
The name to install the service as.

USERNAME / PASSWORD
The account you will install the service under. This account needs administration rights.

* When launching the InstallNlpMailQueueService.bat file, make sure to chose to run this file as administrator.

------------------------------------------------------------------------------------------------------------------------------------------





------------------------------------------------------------------------------------------------------------------------------------------
BATCH UNINSTALL
------------------------------------------------------------------------------------------------------------------------------------------

Similar to the Batch Install, you can run UninstallNlpMailQueueService.bat to uninstall the service. The same three variables are defined
in this batch file. Make sure the path to InstallUtil and the service EXE is correct. Additionally, the SERVICENAME needs to be the same as
the service name used during installation.

* When launching the UninstallNlpMailQueueService.bat file, make sure to chose to run this file as administrator.

------------------------------------------------------------------------------------------------------------------------------------------







INSTALLUTIL
------------------------------------------------------------------------------------------------------------------------------------------

You can additionally install the service by manually running the InstallUtil from the directory the exe is installed to.

	Install Call:
	InstallUtil /username={enter user name here} /password={enter password here} /servicename={enter the desired name of the service} MailQueueProcessor.exe

	Install Sample:
	InstallUtil /username=THENLP\MyUser /password=MyPassword /servicename=MyTestService MailQueueProcessor.exe


You can likewise uninstall the service in a similar fashion, see below:

	UnInstall Call:
	InstallUtil /u /servicename={enter the desired name of the service} MailQueueProcessor.exe

	UnInstall Sample:
	InstallUtil /u /servicename=MyTestService MailQueueProcessor.exe


------------------------------------------------------------------------------------------------------------------------------------------





------------------------------------------------------------------------------------------------------------------------------------------
ADDITIONAL DEPENDENCIES
------------------------------------------------------------------------------------------------------------------------------------------

* This service requires additional SQL files to be present on the database.
* Additionally, GhostScript is required to be installed for Direct Print functionality. There is a GhostScript location preference 
within NorthScope that will be read to determine where GhostScript is installed to on the server.

------------------------------------------------------------------------------------------------------------------------------------------


