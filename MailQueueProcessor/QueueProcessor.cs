﻿
namespace MailQueueProcessor
{
    public class QueueProcessor
    {        
        private INlpQueue Queue { get; set; }        

        public QueueProcessor()
        {            
            Queue = new NlpQueue();
        }


        /// <summary>
        /// Generates a file from available reports to print, 
        /// processes the system mail, and prints direct print ready files.
        /// </summary>
        public void Run()
        {
            //Generate RS Report to File            
            Queue.ProcessReportToFile();

            //Process the queue for records that are ready
            Queue.ProcessMail();

            //Direct Print Ready Files
            Queue.ProcessDirectPrintReadyFiles();
        }

    }
}
