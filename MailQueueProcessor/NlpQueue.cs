﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace MailQueueProcessor
{
    public class NlpQueue : INlpQueue
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string ReportMailQueueSelectProcedure { get; set; }
        private string ReportMailQueueUpdateProcedure { get; set; }
        private string ReportPrinterQueueSelectProcedure { get; set; }
        private string ReportPrinterQueueUpdateProcedure { get; set; }
        private string PrinterQueueSelectProcedure { get; set; }
        private string ProcessSysMailProcedure { get; set; }
        private string GhostScriptPreferenceSelect
        {
            get
            {
                return @"SELECT dbo.FNx_GetSysPrefValue(@DataEntityCompanySK, @ModuleSK, @PreferenceName)";
            }
        }
        private long PrintDocTimeout { get; set; }
        private string GhostScriptPreferenceName { get; set; }
        private string GhostScriptArgumentFormat { get; set; }

        public NlpQueue()
        {
            ReportMailQueueSelectProcedure = ConfigurationManager.AppSettings["ReportMailQueueSelectProcedure"];
            ReportMailQueueUpdateProcedure = ConfigurationManager.AppSettings["ReportMailQueueUpdateProcedure"];
            ReportPrinterQueueSelectProcedure = ConfigurationManager.AppSettings["ReportPrinterQueueSelectProcedure"];
            ReportPrinterQueueUpdateProcedure = ConfigurationManager.AppSettings["ReportPrinterQueueUpdateProcedure"];
            PrinterQueueSelectProcedure = ConfigurationManager.AppSettings["PrinterQueueSelectProcedure"];
            ProcessSysMailProcedure = ConfigurationManager.AppSettings["ProcessSysMailProcedure"];
            PrintDocTimeout = Convert.ToInt64(ConfigurationManager.AppSettings["PrintDocTimeout"]);            
            GhostScriptPreferenceName = ConfigurationManager.AppSettings["GhostScriptPreferenceName"];
            GhostScriptArgumentFormat = ConfigurationManager.AppSettings["GhostScriptArgumentFormat"];
        }

        /// <summary>
        /// Generates RS Reports and Saves to File. ERPx_RSMailQueue is updated to indicate mail queue status and location of file attachment. 
        /// </summary>
        public void ProcessReportToFile()
        {
            long currentRecordSK = 0;    //Used for error handling, so we don't lose the mailqueue SK if an exception is thrown.     
            var currentQueue = Common.RecordQueue.MailQueue;

            using (var selConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
            using (var updConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
            {
                //Mail Queue Select Command
                var selCmd = new SqlCommand()
                {
                    Connection = selConn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ReportMailQueueSelectProcedure
                };

                //Mail Queue Update Command. Add parameters here so they can be updated and reused below. 
                var updCmd = new SqlCommand()
                {
                    Connection = updConn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ReportMailQueueUpdateProcedure
                };

                updCmd.Parameters.Add(new SqlParameter("@MailQueueSK", 0));
                updCmd.Parameters.Add(new SqlParameter("@MailStatus", Common.Status.InProcess));
                updCmd.Parameters.Add(new SqlParameter("@FileAttachments", String.Empty));

                try
                {
                    selConn.Open();
                    updConn.Open();

                    /** MAIL QUEUE **/
                    logger.Trace(string.Format("{0}: Checking for RSMailQueue records. Calling {1}.", DateTime.Now, selCmd.CommandText));
                    var sqlReader = selCmd.ExecuteReader();                                        
                    while(sqlReader.Read())
                    {
                        var mailQueueSK = sqlReader["MailQueueSK"];
                        currentRecordSK = Convert.ToInt64(mailQueueSK);
                        var reportUrl = sqlReader["ReportURL"].ToString();
                        var transactionID = sqlReader["TransactionID"];
                        var reportName = Common.SanitizeName(sqlReader["ReportName"].ToString());
                        var filePath = sqlReader["FilePath"] + @"\";

                        //Update mail queue record to in process                        
                        updCmd.Parameters["@MailQueueSK"].Value = mailQueueSK;
                        updCmd.Parameters["@MailStatus"].Value = Common.Status.InProcess;
                        logger.Trace(string.Format("{0}: Updating RSMailQueue records. Procedure call: {1} {2}", DateTime.Now, updCmd.CommandText, Common.GetParameterList(updCmd)));                                                                                                                               
                        updCmd.ExecuteNonQuery();

                        //Create new pdf file and save
                        var newFile = string.Format("{0}-{1} ({2}).pdf", reportName, transactionID, DateTime.Now.ToString("yyyyMMddHHmmss"));
                        Common.SaveFile(reportUrl, string.Format("{0}{1}", filePath, newFile));  

                        //Update mail queue record to ready and update with name of file. MailQueueSK parameter remains the same.
                        updCmd.Parameters["@MailStatus"].Value = Common.Status.Ready;
                        updCmd.Parameters["@FileAttachments"].Value = newFile;
                        logger.Trace(string.Format("{0}: Updating RSMailQueue records. Procedure call: {1} {2}", DateTime.Now, updCmd.CommandText, Common.GetParameterList(updCmd)));
                        updCmd.ExecuteNonQuery();

                        //Reset parameters
                        updCmd.Parameters["@MailQueueSK"].Value = 0;
                        updCmd.Parameters["@MailStatus"].Value = string.Empty;
                        updCmd.Parameters["@FileAttachments"].Value = string.Empty;                        
                    }
                    sqlReader.Close();


                    /** PRINTER QUEUE **/
                    currentQueue = Common.RecordQueue.PrinterQueue;
                    currentRecordSK = 0;
                    //Reuse select and update commands and connection, change procedures. 
                    selCmd.CommandText = ReportPrinterQueueSelectProcedure;
                    updCmd.CommandText = ReportPrinterQueueUpdateProcedure;
                    updCmd.Parameters.Clear();
                    updCmd.Parameters.Add(new SqlParameter("@PrinterQueueSK", 0));
                    updCmd.Parameters.Add(new SqlParameter("@PrintStatus", Common.Status.InProcess));
                    updCmd.Parameters.Add(new SqlParameter("@FileName", String.Empty));

                    logger.Trace(string.Format("{0}: Checking for RSPrinterQueue records. Calling {1}.", DateTime.Now, selCmd.CommandText));
                    sqlReader = selCmd.ExecuteReader();
                    while(sqlReader.Read())
                    {                        
                        var printerQueueSK = sqlReader["PrinterQueueSK"];
                        currentRecordSK = Convert.ToInt64(printerQueueSK);
                        var requestURL = sqlReader["RequestURL"].ToString();
                        var controlParameterSK = sqlReader["ControlParameterSK"];
                        var reportName = Common.SanitizeName(sqlReader["ReportName"].ToString());
                        var filePath = sqlReader["FolderPath"] + @"\";

                        //Update printer queue record to in process
                        updCmd.Parameters["@PrinterQueueSK"].Value = printerQueueSK;
                        updCmd.Parameters["@PrintStatus"].Value = Common.Status.InProcess;
                        logger.Trace(string.Format("{0}: Updating RSPrinterQueue records. Procedure call: {1} {2}", DateTime.Now, updCmd.CommandText, Common.GetParameterList(updCmd)));
                        updCmd.ExecuteNonQuery();

                        //Create new pdf file and save
                        var newFile = string.Format("{0}-{1} ({2}).pdf", reportName, controlParameterSK, DateTime.Now.ToString("yyyyMMddHHmmss"));
                        Common.SaveFile(requestURL, string.Format("{0}{1}", filePath, newFile));

                        //Update printer queue record to ready and update with name of file. PrinterQueueSK parameter remains the same.
                        updCmd.Parameters["@PrintStatus"].Value = Common.Status.Ready;
                        updCmd.Parameters["@FileName"].Value = newFile;
                        logger.Trace(string.Format("{0}: Updating RSPrinterQueue records. Procedure call: {1} {2}", DateTime.Now, updCmd.CommandText, Common.GetParameterList(updCmd)));
                        updCmd.ExecuteNonQuery();

                        //Reset parameters
                        updCmd.Parameters["@PrinterQueueSK"].Value = 0;
                        updCmd.Parameters["@PrintStatus"].Value = string.Empty;
                        updCmd.Parameters["@FileName"].Value = string.Empty;
                    }
                    sqlReader.Close();
                    selConn.Close();
                    updConn.Close();

                }
                catch (Exception ex)
                {
                    logger.Error(ex, string.Format("Current SK: {0} , Queue: {1}", currentRecordSK, currentQueue));
                    LogErrorToDbRecord(currentRecordSK, currentQueue, ex.ToString());
                }

            }
        }

        /// <summary>
        /// Make a simple connection to SQL and execute procedure that will handle
        ///  the processing of the mail records in the system. 
        /// </summary>
        public void ProcessMail()
        {
            using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
            {
                var sqlCmd = new SqlCommand(ProcessSysMailProcedure, sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    sqlConn.Open();
                    logger.Trace(string.Format("{0}: Processing NLP System Mail. Calling {1}.", DateTime.Now, sqlCmd.CommandText));
                    sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "NlpQueue");
                }
            }
        }

        /// <summary>
        /// Logs the error text provided to the appropriate queue table (Mail or Printer)
        /// </summary>
        /// <param name="currentRecordSK"></param>
        /// <param name="currentQueue"></param>
        /// <param name="errorText"></param>
        public void LogErrorToDbRecord(long currentRecordSK, Common.RecordQueue currentQueue, string errorText)
        {
            if (currentRecordSK > 0)
            {
                using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
                {
                    sqlConn.Open();
                    var sqlCmd = new SqlCommand() { Connection = sqlConn, CommandType = CommandType.StoredProcedure };
                    switch (currentQueue)
                    {
                        case Common.RecordQueue.MailQueue:
                            sqlCmd.CommandText = ReportMailQueueUpdateProcedure;
                            sqlCmd.Parameters.Add(new SqlParameter("@MailQueueSK", currentRecordSK));
                            sqlCmd.Parameters.Add(new SqlParameter("@MailStatus", Common.Status.Error));
                            break;
                        case Common.RecordQueue.PrinterQueue:
                            sqlCmd.CommandText = ReportPrinterQueueUpdateProcedure;
                            sqlCmd.Parameters.Add(new SqlParameter("@PrinterQueueSK", currentRecordSK));
                            sqlCmd.Parameters.Add(new SqlParameter("@PrintStatus", Common.Status.Error));
                            break;
                        default:
                            return;     //Return if we are trying to log to an unsupported location. 
                    }                    
                    sqlCmd.Parameters.Add(new SqlParameter("@ErrorText", errorText));

                    logger.Trace(string.Format("{0}: Logging error to {1}. Procedure call: {2} {3}", DateTime.Now, currentQueue.ToString(), sqlCmd.CommandText, Common.GetParameterList(sqlCmd)));
                    sqlCmd.ExecuteNonQuery();

                    sqlConn.Close();
                }
            }
        }

        /// <summary>
        /// Prints Doc.
        /// </summary>
        /// <param name="info"></param>
        public void PrintDoc(ProcessStartInfo info)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Process printProc = Process.Start(info);
            try
            {
                while (!printProc.HasExited && timer.ElapsedMilliseconds < PrintDocTimeout)
                    Thread.Sleep(50);

                timer.Stop();
                if (timer.ElapsedMilliseconds >= PrintDocTimeout)
                    throw new TimeoutException("Print process timed out");

                if (printProc.ExitCode != 0)
                    throw new InvalidOperationException(string.Format("Printing process exited with a status of {0}", printProc.ExitCode));
            }
            finally
            {
                if (!printProc.HasExited)
                    printProc.Kill();
                printProc.Dispose();
            }
        }

        /// <summary>
        /// Runs Ghost Script to send printer ready files to printer..
        /// </summary>
        public void ProcessDirectPrintReadyFiles()
        {
            long printQueueSK = 0;    //Used for error handling, so we don't lose the SK if an exception is thrown.     

            using (var selConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
            using (var updConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
            using (var gsConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPxConnectionString"].ConnectionString))
            {
                var selCmd = new SqlCommand() 
                { 
                    Connection = selConn, 
                    CommandType = CommandType.StoredProcedure,
                    CommandText = PrinterQueueSelectProcedure
                };

                var updCmd = new SqlCommand()
                {
                    Connection = updConn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ReportPrinterQueueUpdateProcedure
                };
                updCmd.Parameters.Add(new SqlParameter("@PrinterQueueSK", DBNull.Value));           //These will be populated
                updCmd.Parameters.Add(new SqlParameter("@PrintStatus", DBNull.Value));

                var gsSelCmd = new SqlCommand()
                {
                    Connection = gsConn,
                    CommandType = CommandType.Text,
                    CommandText = GhostScriptPreferenceSelect
                };
                gsSelCmd.Parameters.Add(new SqlParameter("@DataEntityCompanySK", DBNull.Value));    //This will be populated
                gsSelCmd.Parameters.Add(new SqlParameter("@ModuleSK", -1));                         //This parameter isn't used but still required.
                gsSelCmd.Parameters.Add(new SqlParameter("@PreferenceName", GhostScriptPreferenceName));
                
                try
                {
                    selConn.Open();
                    updConn.Open();
                    gsConn.Open();
                    var errorList = new StringBuilder();

                    logger.Trace(string.Format("{0}: Checking for print ready records. Calling {1}.", DateTime.Now, selCmd.CommandText));
                    var sqlReader = selCmd.ExecuteReader();

                    while(sqlReader.Read())
                    {
                        printQueueSK = Convert.ToInt64(sqlReader["PrinterQueueSK"]);                        
                        var dataEntityCompanySK = Convert.ToInt64(sqlReader["DataEntityCompanySK"]);
                        var filePath = sqlReader["FolderPath"].ToString();
                        var fileName = sqlReader["FileName"].ToString();
                        var printerName = sqlReader["PrinterName"].ToString();

                        //logger.Trace(string.Format("{0}: Updating RSPrinterQueue records. Procedure call: {1} {2}", DateTime.Now, updCmd.CommandText, Common.GetParameterList(updCmd)));
                        gsSelCmd.Parameters["@DataEntityCompanySK"].Value = dataEntityCompanySK;
                        logger.Trace(string.Format("{0}: Getting preference value for ghost script location. Procedure call: {1} {2}", DateTime.Now, gsSelCmd.CommandText, Common.GetParameterList(gsSelCmd)));
                        var ghostScriptPath = gsSelCmd.ExecuteScalar().ToString();
                                                
                        //Check if there is a preference setting for the GhostScript location at that the exe referenced exists. Do additional validations. 
                        if(string.IsNullOrEmpty(ghostScriptPath) || !File.Exists(ghostScriptPath))
                        {
                            throw new FileNotFoundException("Could not find provided GhostScript location");
                        }

                        if (string.IsNullOrEmpty(filePath))
                        {
                            errorList.Append("File Path cannot be empty. ");
                        }
                        
                        if (string.IsNullOrEmpty(fileName))
                        {
                            errorList.Append("File Attachments cannot be empty. ");
                        }
                        
                        if (string.IsNullOrEmpty(printerName))
                        {
                            errorList.Append("Recipients cannot be empty. ");
                        }
                        
                        if (errorList.Length > 0)
                        {
                            throw new InvalidOperationException(errorList.ToString());
                        }

                        var fileLocation = Path.Combine(filePath, fileName);
                        if (!File.Exists(fileLocation))
                        {
                            throw new FileNotFoundException(string.Format("Could not find provided file: {0}", fileLocation));
                        }

                        var startInfo = new ProcessStartInfo()
                        {
                            FileName = ghostScriptPath,
                            Arguments = string.Format(GhostScriptArgumentFormat, fileLocation, printerName),
                            UseShellExecute = false
                        };
                        logger.Trace(string.Format("{0}: Printing doc. GhostScriptPath: {1}. FileLocation: {2}. PrinterName: {3}. Arguments: {4}.", DateTime.Now, ghostScriptPath, fileLocation, printerName, startInfo.Arguments));
                        PrintDoc(startInfo);

                        updCmd.Parameters["@PrinterQueueSK"].Value = printQueueSK;
                        updCmd.Parameters["@PrintStatus"].Value = Common.Status.Complete;
                        logger.Trace(string.Format("{0}: Updating printer status to complete. Procedure call: {1} {2}", DateTime.Now, updCmd.CommandText, Common.GetParameterList(updCmd)));
                        updCmd.ExecuteNonQuery();

                        //Reset parameters.
                        updCmd.Parameters["@PrinterQueueSK"].Value = DBNull.Value;
                        updCmd.Parameters["@PrintStatus"].Value = DBNull.Value;                        
                        gsSelCmd.Parameters["@DataEntityCompanySK"].Value = DBNull.Value;                        

                    }  // end of sqlReader.Read()


                    selConn.Close();
                    updConn.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, string.Format("PrinterQueueSK: {0} , Queue: {1}", printQueueSK, Common.RecordQueue.PrinterQueue));
                    LogErrorToDbRecord(printQueueSK, Common.RecordQueue.PrinterQueue, ex.ToString());
                }
            }
        }

    }
}
