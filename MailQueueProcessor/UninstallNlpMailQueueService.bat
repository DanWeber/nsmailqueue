@ECHO OFF

set SERVICENAME={enter name for service}
set DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework\v4.0.30319
set EXEPATH={enter path to service install directory}

set PATH=%PATH%;%DOTNETFX2%

echo UnInstalling Service...
echo ---------------------------------------
InstallUtil /servicename=%SERVICENAME%  /u "%EXEPATH%MailQueueProcessor.exe"
echo ---------------------------------------
echo
echo Uninstall complete

pause
echo Done.