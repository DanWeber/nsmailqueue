@ECHO OFF

set SERVICENAME={enter name for service}
set DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework\v4.0.30319
set EXEPATH={enter path to service install directory}
set USERNAME={enter username}
set PASSWORD={enter account password}

set PATH=%PATH%;%DOTNETFX2%

echo Installing Service...
echo ---------------------------------------
InstallUtil /username=%USERNAME% /password=%PASSWORD% /servicename=%SERVICENAME% "%EXEPATH%MailQueueProcessor.exe"
net start %SERVICENAME%
services.msc
echo ---------------------------------------

pause
echo Done.