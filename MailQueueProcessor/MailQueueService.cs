﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using NLog;

namespace MailQueueProcessor
{
    partial class MailQueueService : ServiceBase
    {
        private static Timer ServiceTimer { get; set; }        
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private TimeSpan ServiceInterval { get; set; }

        public MailQueueService()
        {
            ServiceInterval = new TimeSpan(0, 0, Convert.ToInt32(ConfigurationManager.AppSettings["ServiceIntervalSeconds"]));
        }

        /// <summary>
        /// Starts service timer and wires up the time interval elapsed event.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            ServiceTimer = new Timer(ServiceInterval.TotalMilliseconds);     
            ServiceTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimeIntervalElapsed);
            ServiceTimer.Start();            
        }

        protected override void OnStop()
        {
            Program.Stop();
        }

        /// <summary>
        /// Event that fires off when the timer wakes up from it's elapsed time interval.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void OnTimeIntervalElapsed(object source, ElapsedEventArgs e)
        {
            logger.Trace(string.Format("{0}: Service interval of ({1} minutes) has elapsed. Running Program.Start", e.SignalTime, ServiceInterval.TotalMinutes));
            Program.Start(null);
        }
    }
}
