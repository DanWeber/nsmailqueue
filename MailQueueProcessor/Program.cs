﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using NLog;

namespace MailQueueProcessor
{
    class Program
    {      
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string ServiceName = "NLPMailQueueService";

        static void Main(string[] args)
        {         
            if (Environment.UserInteractive)
            {
                logger.Trace(String.Format("{0} Running as exe.", DateTime.Now));                

                // running as console app
                Start(args);

                Console.WriteLine("Press any key to stop...");
                Console.ReadKey(true);

                Stop();                
            }
            else
            {
                // running as service
                using (var service = new MailQueueService())
                {
                    logger.Trace(String.Format("{0} Running as service.", DateTime.Now));
                    ServiceBase.Run(service);
                }
            }
        }

        public static void Start(string[] args)
        {
            logger.Trace(String.Format("{0}: Service started.", DateTime.Now));
            var queueProcessor = new QueueProcessor();
            queueProcessor.Run();
        }

        public static void Stop()
        {
            logger.Trace(String.Format("{0}: Service stopped.", DateTime.Now));            
            NLog.LogManager.Shutdown();
        }
    }
}
