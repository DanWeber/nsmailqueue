﻿using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.ServiceProcess;

namespace MailQueueProcessor
{
    [RunInstaller(true)]
    public class MailQueueServiceInstaller : Installer
    {
        private ServiceInstaller NlpServiceInstaller;
        private ServiceProcessInstaller NlpProcessInstaller;

        /// <summary>
        /// Creates necessary service installer objects and wires up install events. 
        /// </summary>
        public MailQueueServiceInstaller()
        {            
            NlpServiceInstaller = new ServiceInstaller();
            NlpProcessInstaller = new ServiceProcessInstaller();

            this.BeforeInstall += new InstallEventHandler(ServiceInstaller_BeforeInstall);
            NlpServiceInstaller.StartType = ServiceStartMode.Automatic;

            NlpProcessInstaller.Account = ServiceAccount.User;
                       
            Installers.Add(NlpServiceInstaller);
            Installers.Add(NlpProcessInstaller);
        }

        /// <summary>
        /// Reads required arguments for service installation. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ServiceInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            var userName = this.Context.Parameters["username"];
            if (string.IsNullOrEmpty(userName))
            {
                throw new InstallException("Missing parameter 'username'");
            }

            var userPassword = this.Context.Parameters["password"];
            if (string.IsNullOrEmpty(userPassword))
            {
                throw new InstallException("Missing parameter 'password'");
            }

            var serviceName = this.Context.Parameters["servicename"];
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new InstallException("Missing parameter 'servicename'");
            }

            NlpServiceInstaller.ServiceName = serviceName;
            NlpServiceInstaller.DisplayName = serviceName;
            NlpProcessInstaller.Username = userName;
            NlpProcessInstaller.Password = userPassword;
        }

        /// <summary>
        /// Takes into account a parameter to specify the name the service was 
        /// installed as.
        /// </summary>
        /// <param name="savedState"></param>
        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            var serviceName = this.Context.Parameters["servicename"];
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new InstallException("Missing parameter 'servicename'");
            }
            NlpServiceInstaller.ServiceName = serviceName;
            NlpServiceInstaller.DisplayName = serviceName;

            base.OnBeforeUninstall(savedState);
        }
    }
}
