﻿using System;
using System.Diagnostics;
namespace MailQueueProcessor
{
    interface INlpQueue
    {
        void ProcessMail();
        void ProcessReportToFile();
        void ProcessDirectPrintReadyFiles();
    }
}
