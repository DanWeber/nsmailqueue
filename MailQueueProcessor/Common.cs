﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace MailQueueProcessor
{
    public static class Common
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Status used for both MailStatusEN and PrintStatusEN
        /// </summary>
        public enum Status
        {
            New = 1
          , Ready = 2
          , InProcess = 3
          , Complete = 4
          , Error = 5
        }

        /// <summary>
        /// Records process can come from either
        ///  the mail or printer queue tables. 
        /// </summary>
        public enum RecordQueue
        {
              MailQueue = 1
            , PrinterQueue = 2
        }

        /// <summary>
        /// Replaces invalid file name characters.
        /// </summary>
        /// <param name="rawName"></param>
        /// <returns></returns>
        public static string SanitizeName(string rawName)
        {
            foreach (var invalidChar in Path.GetInvalidFileNameChars())
            {
                rawName = rawName.Replace(invalidChar, '_');
            }
            return rawName;
        }

        /// <summary>
        /// Builds out a list of parameter names and values. 
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static string GetParameterList(SqlCommand cmd)
        {
            var sb = new StringBuilder();
            cmd.Parameters.Cast<SqlParameter>()
                          .ToList()
                          .ForEach(p => sb.Append(string.Format(" {0}={1},", p.ParameterName, p.Value)));
            return sb.ToString().Remove(sb.Length - 1, 1);
        }

        /// <summary>
        /// Gets report from reportURL, renames file, and saves to local path.
        /// </summary>
        /// <param name="reportUrl"></param>
        /// <param name="localPath"></param>
        public static void SaveFile(string reportUrl, string localPath)
        {
            byte[] laBytes = new byte[256];
            int liCount = 1;
            var loFileStream = new FileStream(localPath, FileMode.Create, FileAccess.Write);
            var loRequest = (HttpWebRequest)WebRequest.Create(reportUrl);

            loRequest.Credentials = CredentialCache.DefaultCredentials;
            loRequest.ImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Delegation;
            loRequest.Timeout = 900000; //15 min timeout = 900,000 ms
            loRequest.Method = "GET";

            logger.Trace(string.Format("{0}: Getting report from reportUrl {1}.", DateTime.Now, reportUrl));
            var loResponse = (HttpWebResponse)loRequest.GetResponse();
            var loResponseStream = loResponse.GetResponseStream();

            while (liCount > 0)
            {
                liCount = loResponseStream.Read(laBytes, 0, 256);
                loFileStream.Write(laBytes, 0, liCount);
            }

            logger.Trace(string.Format("{0}: Writing pdf to local path {1}.", DateTime.Now, localPath));
            loFileStream.Flush();
            loFileStream.Close();
            System.Threading.Thread.Sleep(2000);
        }
        
    }
}
